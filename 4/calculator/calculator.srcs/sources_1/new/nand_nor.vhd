library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity nand_nor is
	port (	input_0	: in	std_logic_vector (3 downto 0);
		input_1	: in	std_logic_vector (3 downto 0);
		output	: out	std_logic_vector (7 downto 0)
	);
end entity nand_nor;

architecture behavioural of nand_nor is
begin
	-- Addition is only defined for numbers, in this case unsigneds. The result should be an
	-- 8-bit number, so it has to be resized to 8-bit
	output (7 downto 4)	<= not (input_0 and input_1);
	output (3 downto 0) <= not (input_0 or input_1);

end architecture behavioural;
