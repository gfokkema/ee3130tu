library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity compare is
	port (	input_0	: in	std_logic_vector (3 downto 0);
		input_1	: in	std_logic_vector (3 downto 0);
		output	: out	std_logic_vector (7 downto 0)
	);
end entity compare;

architecture behavioural of compare is
begin
    process (input_0, input_1)
    begin
        if input_0 < input_1 then output <= "11111111";
        elsif input_0 > input_1 then output <= "00000000";
        else output <= "00010001";
        end if;
    end process;
end architecture behavioural;
