library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity xor_xnor is
	port (	input_0	: in	std_logic_vector (3 downto 0);
		input_1	: in	std_logic_vector (3 downto 0);
		output	: out	std_logic_vector (7 downto 0)
	);
end entity xor_xnor;

architecture behavioural of xor_xnor is
begin
	-- Addition is only defined for numbers, in this case unsigneds. The result should be an
	-- 8-bit number, so it has to be resized to 8-bit
	output (7 downto 4)	<= input_0 xor input_1;
	output (3 downto 0) <= input_0 xnor input_1;

end architecture behavioural;
