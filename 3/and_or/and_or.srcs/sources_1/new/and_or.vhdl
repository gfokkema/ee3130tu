library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system is
    port ( input_1 : in std_logic;
           input_2 : in std_logic;
           input_3 : in std_logic;
           output_1 : out std_logic);
end system;

architecture structural of system is
    component and_2 is
        port (
            input_1 : in std_logic;
            input_2 : in std_logic;
            output_1 : out std_logic
        );
    end component and_2;
    component or_2 is
        port (
            input_1 : in std_logic;
            input_2 : in std_logic;
            output_1 : out std_logic
        );
    end component or_2;
    signal internal_1 : std_logic;
begin
    lbl0: and_2 port map (
        input_1 => input_1,
        input_2 => input_2,
        output_1 => internal_1
    );
    lbl1: or_2 port map (
        input_1 => internal_1,
        input_2 => input_3,
        output_1 => output_1
    );
end structural;