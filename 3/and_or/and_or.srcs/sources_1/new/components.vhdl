library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity and_2 is
    port (
        input_1 : in std_logic;
        input_2 : in std_logic;
        output_1 : out std_logic
    );
end and_2;

architecture behavioural of and_2 is
begin
    output_1 <= input_1 and input_2;
end behavioural;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity or_2 is
    port (
        input_1 : in std_logic;
        input_2 : in std_logic;
        output_1 : out std_logic
    );
end or_2;

architecture behavioural of or_2 is
begin
    output_1 <= input_1 or input_2;
end behavioural;