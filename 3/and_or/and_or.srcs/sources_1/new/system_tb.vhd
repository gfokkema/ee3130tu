library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_tb is

end system_tb;

architecture structural of system_tb is
    component system is
        port (
            input_1 : in std_logic;
            input_2 : in std_logic;
            input_3 : in std_logic;
            output_1 : out std_logic
        );
    end component system;
    signal input_1 : std_logic;
    signal input_2 : std_logic;
    signal input_3 : std_logic;
    signal output_1 : std_logic;
begin
    input_3 <= '0' after 0 ns,
               '1' after 40 ns;
    input_2 <= '0' after 0 ns,
               '1' after 20 ns,
               '0' after 40 ns,
               '1' after 60 ns;
    input_1 <= '0' after 0 ns,
               '1' after 10 ns,
               '0' after 20 ns,
               '1' after 30 ns,
               '0' after 40 ns,
               '1' after 50 ns,
               '0' after 60 ns,
               '1' after 70 ns;
    lbl0: system port map (
        input_1 => input_1,
        input_2 => input_2,
        input_3 => input_3,
        output_1 => output_1
    );
end ;
