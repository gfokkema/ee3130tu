library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pwm_generator_tb is
end pwm_generator_tb;

architecture structural of pwm_generator_tb is
    component pwm_system is
        port (
            clk : in std_logic;
            reset : in std_logic;
            switch : in std_logic;
            pwm : out std_logic
        );
    end component pwm_system;

    signal clk : std_logic;
    signal reset : std_logic;
    signal switch : std_logic;
    signal pwm : std_logic;
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset <= '1' after 0 ns, '0' after 11 ns;
    switch <= '0' after 0 ms, '1' after 35 ms;

    lbl0: pwm_system port map (
        clk => clk,
        reset => reset,
        switch => switch,
        pwm => pwm
    );

end structural;
