library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity test_controller is
    port (
        clk : in std_logic;
        reset : in std_logic;
        count : in std_logic_vector(20 downto 0);
        pwm_reset : out std_logic
    );
end test_controller;

architecture structural of test_controller is
    type tc_state is (reset_state, wait_state);
    signal state, new_state : tc_state;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= reset_state;
            else
                state <= new_state;
            end if;
        end if;
    end process;

    process (state, count)
    begin
        case state is
            when reset_state =>
                -- Output:
                pwm_reset <= '1';
                -- Input:
                new_state <= wait_state;
            when wait_state =>
                pwm_reset <= '0';
                if unsigned(count) < 2000000 then
                    new_state <= wait_state;
                else
                    new_state <= reset_state;
                end if;
        end case;
    end process;
end structural;
