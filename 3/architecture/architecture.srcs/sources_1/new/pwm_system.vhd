library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pwm_system is
    port (
        clk : in std_logic;
        reset : in std_logic;
        switch : in std_logic;
        pwm : out std_logic
    );
end entity pwm_system;

architecture structural of pwm_system is
    component counter is
        port(
            clk : in std_logic;
            reset : in std_logic;
            count_out : out std_logic_vector (20 downto 0)
        );
    end component counter;
    component pwm_generator is
        port (    
            clk       : in  std_logic;
            reset     : in  std_logic;
            direction : in  std_logic;
            count_in  : in  std_logic_vector (20 downto 0);

            pwm       : out std_logic
        );
    end component pwm_generator;
    component test_controller is
        port (
            clk : in std_logic;
            reset : in std_logic;
            count : in std_logic_vector(20 downto 0);
            pwm_reset : out std_logic
        );
    end component test_controller;

    signal pwm_reset : std_logic;
    signal count : std_logic_vector (20 downto 0);

    type tc_state is (reset_state, wait_state);
    signal state, new_state : tc_state;
begin
    lbl0: counter port map(
        clk => clk,
        reset => pwm_reset,
        count_out => count
    );
    lbl1: pwm_generator port map (
        clk => clk,
        reset => reset,
        direction => switch,
        count_in => count,
        pwm => pwm
    );
    lbl2: test_controller port map (
        clk => clk,
        reset => reset,
        count => count,
        pwm_reset => pwm_reset
    );
end structural;
