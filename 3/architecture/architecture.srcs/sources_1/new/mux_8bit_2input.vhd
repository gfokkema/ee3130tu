library IEEE;
use IEEE.std_logic_1164.all;

entity mux_8bit_2input is
    port (
        sel     : in  std_logic_vector (1 downto 0);
        input_1 : in  std_logic_vector (7 downto 0);
        input_2 : in  std_logic_vector (7 downto 0);
        input_3 : in  std_logic_vector (7 downto 0);
        output  : out std_logic_vector (7 downto 0)
    );
end entity mux_8bit_2input;

architecture behavioural of mux_8bit_2input is
begin
    process (sel, input_1, input_2, input_3)
    begin
        case sel is
            when "00"   => output <= input_1;
            when "01"   => output <= input_2;
            when "10"   => output <= input_3;
            when others => output <= (others => '0');
        end case;
    end process;
end architecture behavioural;