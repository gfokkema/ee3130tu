library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity turn_controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;
        sensors     : in  std_logic_vector (2 downto 0);
        crossing    : out std_logic;
        direction   : out std_logic
    );
end entity turn_controller;

architecture behavioural of turn_controller is
    type c_state is (sleep, signal_right, pending_right, right, signal_left, pending_left, left);
    signal state, new_state : c_state;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= sleep;
            else
                state <= new_state;
            end if;
        end if;
    end process;

    process (state, sensors)
    begin
        crossing  <= '0';
        direction <= '0';
        -- Defaults: we don't need to stop in this controller
        case state is
            when sleep =>
                case sensors is
                    when "010"  => new_state <= signal_right;
                    when others => new_state <= sleep;
                end case;
            when signal_right =>
                case sensors is
                    when "010"  => new_state <= signal_right;
                    when others => new_state <= pending_right;
                end case;
            when pending_right =>
                case sensors is
                    when "000"  => new_state <= right;
                    when "010"  => new_state <= signal_left;
                    when others => new_state <= pending_right;
                end case;
            when right =>
                crossing  <= '1';
                direction <= '0';
                new_state <= right;
            when signal_left =>
                case sensors is
                    when "010"  => new_state <= signal_left;
                    when others => new_state <= pending_left;
                end case;
            when pending_left =>
                case sensors is
                    when "000"  => new_state <= left;
                    when others => new_state <= pending_left;
                end case;
            when left =>
                crossing  <= '1';
                direction <= '1';
                new_state <= left;
        end case;
    end process;
end architecture behavioural;
