library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity track_controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;
        crossing    : in  std_logic;
        sensors     : in  std_logic_vector (2 downto 0);
        count_in    : in  std_logic_vector (20 downto 0);

        count_reset       : out std_logic;
        motor_l_reset     : out std_logic;
        motor_l_direction : out std_logic;
        motor_r_reset     : out std_logic;
        motor_r_direction : out std_logic;

        done              : out std_logic;
        leds              : out std_logic_vector (7 downto 0)
    );
end entity track_controller;

architecture behavioural of track_controller is
    constant MILLIS_20 : integer := 2000000;

    type c_state is (sleep, pendingstop, pendingstopfwd, forward, left, right, sharpleft, sharpright, finished);
    signal state, new_state : c_state;
    signal stopcount, new_stopcount : unsigned (5 downto 0);
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= sleep;
                stopcount <= (others => '0');
            else
                state <= new_state;
                stopcount <= new_stopcount;
            end if;
        end if;
    end process;

    process (state, count_in, sensors)
    begin
        -- Defaults: we don't need to stop in this controller
        count_reset       <= '0';
        motor_l_reset     <= '0';
        motor_r_reset     <= '0';
        motor_l_direction <= '1';
        motor_r_direction <= '0';

        done              <= '0';
        leds              <= "00000000";
        new_stopcount     <= stopcount;

        case state is
            when sleep =>
                -- Output:
                count_reset <= '1';
                new_stopcount <= (others => '0');
                -- Input:
                if crossing = '1' then
                    new_state <= finished;
                else
                    case sensors is
                        when "000"|"010"|"101" => new_state <= forward;
                        when "111"  => new_state <= pendingstop;
                        when "001"  => new_state <= left;
                        when "011"  => new_state <= sharpleft;
                        when "100"  => new_state <= right;
                        when "110"  => new_state <= sharpright;
                        when others => new_state <= sleep;
                    end case;
                end if;
            when pendingstop =>
                leds(5) <= '1';
                -- Output:
                count_reset <= '1';
                new_stopcount <= stopcount + 1;
                -- Input:
                if unsigned(stopcount) > 50 then
                    new_state <= finished;
                else
                    case sensors is
                        when "111"  => new_state <= pendingstopfwd;
                        when others => new_state <= sleep;
                    end case;
                end if;
            when pendingstopfwd =>
                leds(5) <= '1';
                -- Output:
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= pendingstopfwd;
                else
                    new_state <= pendingstop;
                end if;            
            when forward =>
                leds(0) <= '1';
                -- Output:
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= forward;
                else
                    new_state <= sleep;
                end if;
            when left =>
                leds(1) <= '1';
                -- Output:
                motor_l_reset <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= left;
                else
                    new_state <= sleep;
                end if;
            when sharpleft =>
                leds(2) <= '1';
                -- Output:
                motor_l_direction <= '0';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= sharpleft;
                else
                    new_state <= sleep;
                end if;
            when right =>
                leds(3) <= '1';
                -- Output:
                motor_r_reset <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= right;
                else
                    new_state <= sleep;
                end if;
            when sharpright =>
                leds(4) <= '1';
                -- Output:
                motor_r_direction <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= sharpright;
                else
                    new_state <= sleep;
                end if;
            when finished =>
                done <= '1';
                new_state <= finished;
        end case;
    end process;
end architecture behavioural;
