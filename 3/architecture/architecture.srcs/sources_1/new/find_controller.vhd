library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity find_controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;
        sensors     : in  std_logic_vector (2 downto 0);
        count_in    : in  std_logic_vector (20 downto 0);

        count_reset       : out std_logic;

        motor_l_reset     : out std_logic;
        motor_l_direction : out std_logic;

        motor_r_reset     : out std_logic;
        motor_r_direction : out std_logic;

        done              : out std_logic;
        leds              : out std_logic_vector (7 downto 0)
    );
end entity find_controller;

architecture behavioural of find_controller is
    constant MILLIS_20 : integer := 2000000;

    type c_state is (find, findfwd, pass, passfwd, store, turning, left, right, found, finished);
    signal state, new_state : c_state;
    signal last_input, new_last_input : std_logic_vector (2 downto 0);
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state      <= find;
                last_input <= "111";
            else
                state      <= new_state;
                last_input <= new_last_input;
            end if;
        end if;
    end process;

    process (state, count_in, sensors, last_input)
    begin
        -- Defaults: we don't need to stop in this controller
        new_last_input    <= last_input;

        count_reset       <= '0';
        motor_l_reset     <= '0';
        motor_r_reset     <= '0';
        motor_l_direction <= '1';
        motor_r_direction <= '0';

        done              <= '0';
        leds              <= last_input & "00000";

        case state is
            when find => 
                leds(0) <= '1';
                -- Output:
                count_reset <= '1';
                -- Input:
                case sensors is
                    when "111"  => new_state <= findfwd;
                    when "101"  => new_state <= found;
                    when others => new_state <= pass;
                end case;
            when findfwd =>
                leds(0) <= '1';
                -- Output:
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= findfwd;
                else
                    new_state <= find;
                end if;
            when pass =>
                leds(1) <= '1';
                -- Output:
                count_reset <= '1';
                -- Input:
                case sensors is
                    when "111"  => new_state <= turning;
                    when others => new_state <= store;
                end case;
            when passfwd =>
                leds(1) <= '1';
                -- Output:
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= passfwd;
                else
                    new_state <= pass;
                end if;
            when store =>
                leds(1) <= '1';
                -- Output:
                new_last_input <= sensors;
                -- Input:
                new_state <= passfwd;
            when turning =>
                -- Output:
                count_reset   <= '1';
                motor_l_reset <= '1';
                motor_r_reset <= '1';
                -- Input:
                case last_input is
                    when "011" | "001" => new_state <= left;
                    when "110" | "100" => new_state <= right;
                    when others        => new_state <= right;
                end case;
            when left =>
                leds(2) <= '1';
                -- Output:
                motor_l_reset <= '1';
                -- Input:
                if sensors = "101" then
                    new_state <= found;
                elsif unsigned(count_in) < MILLIS_20 then
                    new_state <= left;
                else
                    new_state <= turning;
                end if;
            when right =>
                leds(3) <= '1';
                -- Output:
                motor_r_reset <= '1';
                -- Input:
                if sensors = "101" then
                    new_state <= found;
                elsif unsigned(count_in) < MILLIS_20 then
                    new_state <= right;
                else
                    new_state <= turning;
                end if;
            when found =>
                leds(4) <= '1';
                -- Output:
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= found;
                else
                    new_state <= finished;
                end if;
            when finished =>
                leds(4) <= '1';
                -- Output:
                count_reset   <= '1';
                done          <= '1';
                motor_l_reset <= '1';
                motor_r_reset <= '1';
                -- Input:
                new_state <= finished;
        end case;
    end process;
end architecture behavioural;