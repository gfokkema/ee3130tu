library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity simple_controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;

        sensor_l    : in  std_logic;
        sensor_m    : in  std_logic;
        sensor_r    : in  std_logic;

        count_in    : in  std_logic_vector (20 downto 0);
        count_reset : out std_logic;

        motor_l_reset     : out    std_logic;
        motor_l_direction : out    std_logic;

        motor_r_reset     : out    std_logic;
        motor_r_direction : out    std_logic
    );
end entity simple_controller;

architecture structural of simple_controller is
    type c_state is (off, forward);
    signal state, new_state : c_state;
begin
    motor_l_direction <= '1';
    motor_r_direction <= '0';

    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= off;
            else
                state <= new_state;
            end if;
        end if;
    end process;

    process (state, count_in, sensor_l, sensor_m, sensor_r)
    begin
        case state is
            when off =>
                -- Output:
                motor_l_reset <= '1';
                motor_r_reset <= '1';
                count_reset <= '1';
                -- Input:
                if sensor_l = '1' and sensor_m = '0' and sensor_r = '1' then
                    new_state <= forward;
                else
                    new_state <= off;
                end if;
            when forward =>
                -- Output:
                motor_l_reset <= '0';
                motor_r_reset <= '0';
                count_reset <= '0';
                -- Input:
                if unsigned(count_in) < 2000000 then
                    new_state <= forward;
                else
                    new_state <= off;
                end if;
        end case;
    end process;
end architecture structural;
