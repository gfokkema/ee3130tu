library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity main_controller is
    port (
        clk           : in  std_logic;
        reset         : in  std_logic;
        find_reset    : out std_logic;
        find_done     : in  std_logic;
        track_reset   : out std_logic;
        track_done    : in  std_logic;
        turn_reset    : out std_logic;
        turn_crossing : in  std_logic;
        rotate_reset  : out std_logic;
        rotate_done   : in  std_logic;
        sel           : out std_logic_vector (1 downto 0)
    );
end main_controller;

architecture behavioural of main_controller is
    type c_state is (init, find, track, rotate, finished);
    signal state, new_state : c_state;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= init;
            else
                state <= new_state;
            end if;
        end if;
    end process;

    process (state, find_done, track_done, turn_crossing, rotate_done)
    begin
        case state is
            when init =>
                -- Output:
                sel          <= "11";
                find_reset   <= '1';
                track_reset  <= '1';
                turn_reset   <= '1';
                rotate_reset <= '1';
                -- Input:
                new_state <= find;
            when find =>
                -- Output:
                sel          <= "00";
                find_reset   <= '0';
                track_reset  <= '1';
                turn_reset   <= '1';
                rotate_reset <= '1';
                -- Input:
                case find_done is
                    when '1'    => new_state <= track;
                    when others => new_state <= find;
                end case;
            when track =>
                -- Output:
                sel          <= "01";
                find_reset   <= '1';
                track_reset  <= '0';
                turn_reset   <= '0';
                rotate_reset <= '1';
                -- Input:
                case track_done is
                    when '1'    =>
                        case turn_crossing is
                            when '1'    => new_state <= rotate;
                            when others => new_state <= finished;
                        end case;
                    when others => new_state <= track;
                end case;
            when rotate =>
                -- Output:
                sel          <= "10";
                find_reset   <= '1';
                track_reset  <= '1';
                turn_reset   <= '1';
                rotate_reset <= '0';
                 -- Input:
                case rotate_done is
                    when '1'    => new_state <= track;
                    when others => new_state <= rotate;
                end case;
            when finished =>
                -- Output:
                sel          <= "11";
                find_reset   <= '1';
                track_reset  <= '1';
                turn_reset   <= '1';
                rotate_reset <= '1';
                -- Input:
                new_state <= finished;
        end case;
    end process;
end behavioural;
