library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rotate_controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;
        sensors     : in  std_logic_vector (2 downto 0);
        direction   : in  std_logic;
        count_in    : in  std_logic_vector (20 downto 0);

        count_reset       : out std_logic;
        motor_l_reset     : out std_logic;
        motor_l_direction : out std_logic;
        motor_r_reset     : out std_logic;
        motor_r_direction : out std_logic;

        done              : out std_logic;
        leds              : out std_logic_vector (7 downto 0)
    );
end entity rotate_controller;

architecture behavioural of rotate_controller is
    constant MILLIS_20 : integer := 2000000;

    type c_state is (sleep, wait_left, wait_right, wait_sharpleft, wait_sharpright, left, right, sharpleft, sharpright, finished);
    signal state, new_state : c_state;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= sleep;
            else
                state <= new_state;
            end if;
        end if;
    end process;

    process (state, count_in, sensors, direction)
    begin
        -- Defaults: we don't need to stop in this controller
        count_reset       <= '0';
        motor_l_reset     <= '0';
        motor_r_reset     <= '0';
        motor_l_direction <= '1';
        motor_r_direction <= '0';

        done              <= '0';
        leds              <= "00000000";

        case state is
            when sleep =>
                -- Output:
                count_reset   <= '1';
                motor_l_reset <= '1';
                motor_r_reset <= '1';
                -- Input:
                case direction is
                    when '0'    => new_state <= wait_right;
                    when others => new_state <= wait_left;
                    
                end case;
            when wait_left =>
                -- Output:
                count_reset       <= '1';
                -- Input:
                case sensors is
                    when "111"  => new_state <= sharpleft;
                    when others => new_state <= left;
                end case;
            when left =>
                -- Output:
                motor_l_reset <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= left;
                else
                    new_state <= wait_left;
                end if;
            when wait_sharpleft =>
                -- Output:
                count_reset       <= '1';
                motor_l_direction <= '0';
                -- Input:
                case sensors is
                    when "101"  => new_state <= finished;
                    when others => new_state <= sharpleft;
                end case;
            when sharpleft =>
                -- Output:
                motor_l_direction <= '0';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= sharpleft;
                else
                    new_state <= wait_sharpleft;
                end if;
            when wait_right =>
                -- Output:
                count_reset       <= '1';
                -- Input:
                case sensors is
                    when "111"  => new_state <= sharpright;
                    when others => new_state <= right;
                end case;
            when right =>
                -- Output:
                motor_r_reset <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= right;
                else
                    new_state <= wait_right;
                end if;
            when wait_sharpright =>
                -- Output:
                count_reset       <= '1';
                -- Input:
                case sensors is
                    when "101"  => new_state <= finished;
                    when others => new_state <= sharpright;
                end case;
            when sharpright =>
                -- Output:
                motor_r_direction <= '1';
                -- Input:
                if unsigned(count_in) < MILLIS_20 then
                    new_state <= sharpright;
                else
                    new_state <= wait_sharpright;
                end if;
            when finished =>
                done <= '1';
                new_state <= finished;
        end case;
    end process;
end architecture behavioural;
