library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity input_buffer is
    port (    
        clk          : in    std_logic;

        sensor_l_in  : in    std_logic;
        sensor_m_in  : in    std_logic;
        sensor_r_in  : in    std_logic;

        sensor_l_out : out    std_logic;
        sensor_m_out : out    std_logic;
        sensor_r_out : out    std_logic
    );
end entity input_buffer;

architecture behavioural of input_buffer is
    signal sensor_in : std_logic_vector(2 downto 0);
    signal sensor_between : std_logic_vector(2 downto 0);
    signal sensor_out : std_logic_vector(2 downto 0);
begin
    sensor_in <= sensor_l_in & sensor_m_in & sensor_r_in;
    sensor_l_out <= sensor_out(2);
    sensor_m_out <= sensor_out(1);
    sensor_r_out <= sensor_out(0);

    process (clk)
    begin
        if rising_edge(clk) then
            sensor_between <= sensor_in;
            sensor_out <= sensor_between;
        else
            sensor_between <= sensor_between;
            sensor_out <= sensor_out;
        end if;
    end process;
end architecture behavioural;
