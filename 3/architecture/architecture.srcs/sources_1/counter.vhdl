library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- The counter should count to 100 MHz / 50 Hz = 2 M.
-- Therefore it should have at least 21 bits.
entity counter is
    port(
        clk : in std_logic;
        reset : in std_logic;
        count_out : out std_logic_vector (20 downto 0)
    );
end counter;

architecture behavioural of counter is
    signal count, new_count : unsigned (20 downto 0);
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                count <= (others => '0');
            else
                count <= new_count;
            end if;
        end if;
    end process;
    
    process (count)
    begin
        new_count <= count + 1;
    end process;
    
    count_out <= std_logic_vector(count);
end behavioural;
