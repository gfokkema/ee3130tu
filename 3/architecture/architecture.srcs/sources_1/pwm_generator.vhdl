library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pwm_generator is
    port (    
        clk       : in  std_logic;
        reset     : in  std_logic;
        direction : in  std_logic;
        count_in  : in  std_logic_vector (20 downto 0);

        pwm       : out std_logic
    );
end entity pwm_generator;

architecture behavioural of pwm_generator is
    constant MILLIS_1: integer := 100000;
    constant MILLIS_2: integer := 200000;

    type pwm_state is (off, forward, reverse);
    signal state, new_state : pwm_state;
begin
    process (clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                state <= off;
            else
                state <= new_state;
            end if;
        end if;
    end process;
    
    process (state, direction, count_in)
    begin
        case state is
            when off =>
                -- Output:
                pwm <= '0';
                -- Input:
                if unsigned(count_in) > MILLIS_1 then
                    new_state <= off;
                else
                    if direction = '1' then
                        new_state <= forward;
                    else
                        new_state <= reverse;
                    end if;
                end if;
            when forward =>
                pwm <= '1';
                if unsigned(count_in) > MILLIS_2 then
                    new_state <= off;
                else
                    new_state <= forward;
                end if;
            when reverse =>
                pwm <= '1';
                if unsigned(count_in) > MILLIS_1 then
                    new_state <= off;
                else
                    new_state <= reverse;
                end if;
        end case;
    end process;
end architecture behavioural;