library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity controller is
    port (
        clk         : in  std_logic;
        reset       : in  std_logic;

        sensor_l    : in  std_logic;
        sensor_m    : in  std_logic;
        sensor_r    : in  std_logic;

        count_in    : in  std_logic_vector (20 downto 0);

        count_reset       : out std_logic;

        motor_l_reset     : out std_logic;
        motor_l_direction : out std_logic;

        motor_r_reset     : out std_logic;
        motor_r_direction : out std_logic;

        leds              : out std_logic_vector (15 downto 0)
    );
end entity controller;

architecture structural of controller is
    component main_controller is
        port (
            clk           : in  std_logic;
            reset         : in  std_logic;
            find_reset    : out std_logic;
            find_done     : in  std_logic;
            track_reset   : out std_logic;
            track_done    : in  std_logic;
            turn_reset    : out std_logic;
            turn_crossing : in  std_logic;
            rotate_reset  : out std_logic;
            rotate_done   : in  std_logic;
            sel           : out std_logic_vector ( 1 downto 0)
        );
    end component main_controller;
    component mux_5bit_3input is
        port (
            sel     : in  std_logic_vector (1 downto 0);
            input_1 : in  std_logic_vector (4 downto 0);
            input_2 : in  std_logic_vector (4 downto 0);
            input_3 : in  std_logic_vector (4 downto 0);
            output  : out std_logic_vector (4 downto 0)
        );
    end component mux_5bit_3input;
    component mux_8bit_2input is
        port (
            sel     : in  std_logic_vector (1 downto 0);
            input_1 : in  std_logic_vector (7 downto 0);
            input_2 : in  std_logic_vector (7 downto 0);
            input_3 : in  std_logic_vector (7 downto 0);
            output  : out std_logic_vector (7 downto 0)
        );
    end component mux_8bit_2input;
    component find_controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensors     : in  std_logic_vector (2 downto 0);
            count_in    : in  std_logic_vector (20 downto 0);

            count_reset       : out std_logic;
            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;
            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;

            done              : out std_logic;
            leds              : out std_logic_vector (7 downto 0)
    );
    end component find_controller;
    component track_controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            crossing    : in  std_logic;
            sensors     : in  std_logic_vector (2 downto 0);
            count_in    : in  std_logic_vector (20 downto 0);

            count_reset       : out std_logic;
            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;
            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;

            done              : out std_logic;
            leds              : out std_logic_vector (7 downto 0)
        );
    end component track_controller;
    component turn_controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensors     : in  std_logic_vector (2 downto 0);
            crossing    : out std_logic;
            direction   : out std_logic
        );
    end component turn_controller;
    component rotate_controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensors     : in  std_logic_vector (2 downto 0);
            direction   : in  std_logic;
            count_in    : in  std_logic_vector (20 downto 0);
    
            count_reset       : out std_logic;
            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;
            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;
    
            done              : out std_logic;
            leds              : out std_logic_vector (7 downto 0)
        );
    end component rotate_controller;

    signal sensors      : std_logic_vector (2 downto 0);

    signal find_reset   : std_logic;
    signal find_done    : std_logic;
    signal find_leds    : std_logic_vector (7 downto 0);
    signal find_output  : std_logic_vector (4 downto 0);

    signal track_reset  : std_logic;
    signal track_done   : std_logic;
    signal track_leds   : std_logic_vector (7 downto 0);
    signal track_output : std_logic_vector (4 downto 0);

    signal turn_reset     : std_logic;
    signal turn_crossing  : std_logic;
    signal turn_direction : std_logic;
    
    signal rotate_reset   : std_logic;
    signal rotate_done    : std_logic;
    signal rotate_leds    : std_logic_vector (7 downto 0);
    signal rotate_output  : std_logic_vector (4 downto 0);

    signal sel          : std_logic_vector (1 downto 0);
    signal output       : std_logic_vector (4 downto 0);
begin
    sensors <= sensor_l & sensor_m & sensor_r;
    leds (15 downto 8) <= reset & sel & "00000";

    count_reset       <= output(4);
    motor_l_reset     <= output(3);
    motor_l_direction <= output(2);
    motor_r_reset     <= output(1);
    motor_r_direction <= output(0);

    lbl_main_ctrl: main_controller port map (
        clk           => clk,
        reset         => reset,
        find_reset    => find_reset,
        find_done     => find_done,
        track_reset   => track_reset,
        track_done    => track_done,
        turn_reset    => turn_reset,
        turn_crossing => turn_crossing,
        rotate_reset  => rotate_reset,
        rotate_done   => rotate_done,
        sel           => sel
    );
    lbl_find_ctrl: find_controller port map (
        clk          => clk,
        reset        => find_reset,
        sensors      => sensors,
        count_in     => count_in,
        count_reset       => find_output(4),
        motor_l_reset     => find_output(3),
        motor_l_direction => find_output(2),
        motor_r_reset     => find_output(1),
        motor_r_direction => find_output(0),
        done              => find_done,
        leds              => find_leds
    );
    lbl_track_ctrl: track_controller port map (
        clk          => clk,
        reset        => track_reset,
        crossing     => turn_crossing,
        sensors      => sensors,
        count_in     => count_in,
        count_reset       => track_output(4),
        motor_l_reset     => track_output(3),
        motor_l_direction => track_output(2),
        motor_r_reset     => track_output(1),
        motor_r_direction => track_output(0),
        done              => track_done,
        leds              => track_leds
    );
    lbl_turn_ctrl: turn_controller port map (
        clk          => clk,
        reset        => track_reset,
        sensors      => sensors,
        crossing     => turn_crossing,
        direction    => turn_direction
    );
    lbl_rotate_ctrl: rotate_controller port map (
        clk          => clk,
        reset        => rotate_reset,
        sensors      => sensors,
        direction    => turn_direction,
        count_in     => count_in,
        count_reset       => rotate_output(4),
        motor_l_reset     => rotate_output(3),
        motor_l_direction => rotate_output(2),
        motor_r_reset     => rotate_output(1),
        motor_r_direction => rotate_output(0),
        done              => rotate_done,
        leds              => rotate_leds
    );
    lbl_mux: mux_5bit_3input port map (
        sel     => sel,
        input_1 => find_output,
        input_2 => track_output,
        input_3 => rotate_output,
        output  => output
    );
    lbl_mux_leds: mux_8bit_2input port map (
        sel     => sel,
        input_1 => find_leds,
        input_2 => track_leds,
        input_3 => rotate_leds,
        output  => leds (7 downto 0)
    );
end architecture structural;
