library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system is
    port (
        clk      : in std_logic;
        sw       : in std_logic_vector (15 downto 0);
        sensor_l : in std_logic;
        sensor_m : in std_logic;
        sensor_r : in std_logic;
        reset    : in std_logic;
        pwm_l    : out std_logic;
        pwm_r    : out std_logic;
        leds     : out std_logic_vector (15 downto 0)
    );
end entity system;

architecture structural of system is
    component input_buffer is
        port (
            clk          : in  std_logic;
            
            sensor_l_in  : in  std_logic;
            sensor_m_in  : in  std_logic;
            sensor_r_in  : in  std_logic;
            
            sensor_l_out : out std_logic;
            sensor_m_out : out std_logic;
            sensor_r_out : out std_logic
        );
    end component input_buffer;
    component controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            
            sensor_l    : in  std_logic;
            sensor_m    : in  std_logic;
            sensor_r    : in  std_logic;
    
            count_in    : in  std_logic_vector (20 downto 0);
            count_reset : out std_logic;
    
            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;
    
            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;
            
            leds              : out std_logic_vector (15 downto 0)
        );
    end component controller;
    component counter is
        port (
            clk       : in  std_logic;
            reset     : in  std_logic;

            count_out : out std_logic_vector (20 downto 0)
        );
    end component counter;
    component pwm_generator is
        port (
            clk       : in  std_logic;
            reset     : in  std_logic;

            direction : in  std_logic;
            count_in  : in  std_logic_vector (20 downto 0);
    
            pwm       : out std_logic
        );
    end component pwm_generator;

    signal main_reset        : std_logic;
    signal sensor_l_bufout   : std_logic;
    signal sensor_m_bufout   : std_logic;
    signal sensor_r_bufout   : std_logic;
    signal count_reset       : std_logic;
    signal count_out         : std_logic_vector (20 downto 0);
    signal motor_l_direction : std_logic;
    signal motor_l_reset     : std_logic;
    signal motor_r_direction : std_logic;
    signal motor_r_reset     : std_logic;
begin
    main_reset <= reset or sw(0);
    lbl_buffer: input_buffer port map (
        clk          => clk,
        sensor_l_in  => sensor_l,
        sensor_m_in  => sensor_m,
        sensor_r_in  => sensor_r,
        sensor_l_out => sensor_l_bufout,
        sensor_m_out => sensor_m_bufout,
        sensor_r_out => sensor_r_bufout
    );
    lbl_ctrl: controller port map (
        clk          => clk,
        reset        => main_reset,
        sensor_l     => sensor_l_bufout,
        sensor_m     => sensor_m_bufout,
        sensor_r     => sensor_r_bufout,
        count_in     => count_out,
        count_reset  => count_reset,
        motor_l_reset     => motor_l_reset,
        motor_l_direction => motor_l_direction,
        motor_r_reset     => motor_r_reset,
        motor_r_direction => motor_r_direction,
        leds              => leds
    );
    lbl_counter: counter port map (
        clk       => clk,
        reset     => count_reset,
        count_out => count_out
    );
    lbl_pwm_l: pwm_generator port map (
        clk       => clk,
        reset     => motor_l_reset,
        direction => motor_l_direction,
        count_in  => count_out,
        pwm       => pwm_l
    );
    lbl_pwm_r: pwm_generator port map (
        clk       => clk,
        reset     => motor_r_reset,
        direction => motor_r_direction,
        count_in  => count_out,
        pwm       => pwm_r
    );
end architecture structural;
