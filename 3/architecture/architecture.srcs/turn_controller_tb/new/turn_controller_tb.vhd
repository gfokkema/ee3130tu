library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity turn_controller_tb is
end turn_controller_tb;

architecture structural of turn_controller_tb is
    component counter is
        port(
            clk       : in  std_logic;
            reset     : in  std_logic;
            count_out : out std_logic_vector (20 downto 0)
        );
    end component counter;
    component controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensor_l    : in  std_logic;
            sensor_m    : in  std_logic;
            sensor_r    : in  std_logic;
            count_in    : in  std_logic_vector (20 downto 0);

            count_reset       : out std_logic;

            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;

            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;
            
            leds : out std_logic_vector (15 downto 0)
        );
    end component controller;

    signal clk         : std_logic;
    signal reset       : std_logic;
    signal sensors     : std_logic_vector (2 downto 0);
    signal count       : std_logic_vector (20 downto 0);
    signal count_reset : std_logic;
    signal motor_l_reset     : std_logic;
    signal motor_l_direction : std_logic;
    signal motor_r_reset     : std_logic;
    signal motor_r_direction : std_logic;
    signal leds : std_logic_vector (15 downto 0);
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset <= '1' after 0 ns, '0' after 11 ns;
    sensors <= "101" after 0 ms, "101" after 20 ms,    -- forward, forward
               "101" after 40 ms, "000" after 60 ms,   -- forward, start of turn
               "010" after 80 ms, "010" after 100 ms,  -- turn right, turn right
               "111" after 120 ms, "111" after 140 ms, -- white, white
               "101" after 160 ms, "101" after 180 ms, -- line, line
               "010" after 200 ms, "010" after 220 ms, -- turn left, turn left
               "111" after 240 ms, "111" after 260 ms, -- white, white
               "000" after 270 ms, "101" after 300 ms; -- crossing, line

    lbl0: counter port map (
        clk       => clk,
        reset     => count_reset,
        count_out => count
    );
    lbl1: controller port map (
        clk         => clk,
        reset       => reset,
        sensor_l    => sensors(2),
        sensor_m    => sensors(1),
        sensor_r    => sensors(0),
        count_in    => count,
        count_reset => count_reset,
        motor_l_reset     => motor_l_reset,
        motor_l_direction => motor_l_direction,
        motor_r_reset     => motor_r_reset,
        motor_r_direction => motor_r_direction,
        leds => leds
    );
end structural;
