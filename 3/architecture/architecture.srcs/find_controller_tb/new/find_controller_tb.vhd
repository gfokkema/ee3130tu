library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity find_controller_tb is
end find_controller_tb;

architecture structural of find_controller_tb is
    component counter is
        port(
            clk       : in  std_logic;
            reset     : in  std_logic;
            count_out : out std_logic_vector (20 downto 0)
        );
    end component counter;
    component find_controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensors     : in  std_logic_vector (2 downto 0);
            count_in    : in  std_logic_vector (20 downto 0);

            count_reset       : out std_logic;

            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;

            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;
            
            done : out std_logic;
            leds : out std_logic_vector (7 downto 0)
        );
    end component find_controller;

    signal clk         : std_logic;
    signal reset       : std_logic;
    signal sensors     : std_logic_vector (2 downto 0);
    signal count       : std_logic_vector (20 downto 0);
    signal count_reset : std_logic;
    signal motor_l_reset     : std_logic;
    signal motor_l_direction : std_logic;
    signal motor_r_reset     : std_logic;
    signal motor_r_direction : std_logic;
    signal done : std_logic;
    signal leds : std_logic_vector (7 downto 0);
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset <= '1' after 0 ns, '0' after 11 ns;
    sensors <= "111" after 0 ms, "011" after 40 ms, "001" after 60 ms,
               "100" after 80 ms, "110" after 100 ms, "111" after 120 ms, "101" after 140 ms;

    lbl0: counter port map (
        clk       => clk,
        reset     => count_reset,
        count_out => count
    );
    lbl1: find_controller port map (
        clk         => clk,
        reset       => reset,
        sensors     => sensors,
        count_in    => count,
        count_reset => count_reset,
        motor_l_reset     => motor_l_reset,
        motor_l_direction => motor_l_direction,
        motor_r_reset     => motor_r_reset,
        motor_r_direction => motor_r_direction,
        done => done,
        leds => leds
    );
end structural;
