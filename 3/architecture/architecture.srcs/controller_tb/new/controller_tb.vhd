library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity controller_tb is
end controller_tb;

architecture structural of controller_tb is
    component counter is
        port(
            clk       : in  std_logic;
            reset     : in  std_logic;
            count_out : out std_logic_vector (20 downto 0)
        );
    end component counter;
    component controller is
        port (
            clk         : in  std_logic;
            reset       : in  std_logic;
            sensor_l    : in  std_logic;
            sensor_m    : in  std_logic;
            sensor_r    : in  std_logic;
            count_in    : in  std_logic_vector (20 downto 0);

            count_reset       : out std_logic;

            motor_l_reset     : out std_logic;
            motor_l_direction : out std_logic;

            motor_r_reset     : out std_logic;
            motor_r_direction : out std_logic;
            
            leds : out std_logic_vector (15 downto 0)
        );
    end component controller;
    component pwm_generator is
        port (
            clk       : in  std_logic;
            reset     : in  std_logic;

            direction : in  std_logic;
            count_in  : in  std_logic_vector (20 downto 0);
    
            pwm       : out std_logic
        );
    end component pwm_generator;

    signal clk         : std_logic;
    signal reset       : std_logic;
    signal sensors     : std_logic_vector (2 downto 0);
    signal count       : std_logic_vector (20 downto 0);
    signal count_reset : std_logic;
    signal motor_l_reset     : std_logic;
    signal motor_l_direction : std_logic;
    signal motor_r_reset     : std_logic;
    signal motor_r_direction : std_logic;
    signal pwm_l             : std_logic;
    signal pwm_r             : std_logic;
    signal leds : std_logic_vector (15 downto 0);
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset <= '1' after 0 ns, '0' after 11 ns;
    sensors <= "111" after 0 ms, "011" after 40 ms,    -- find, pass
               "001" after 60 ms, "100" after 80 ms,   -- pass, pass
               "110" after 100 ms, "111" after 120 ms, -- pass, right
               "111" after 140 ms, "101" after 150 ms, -- right, finished
               "101" after 180 ms, "011" after 200 ms, -- forward, sharpleft
               "001" after 220 ms, "110" after 240 ms, -- left, right
               "100" after 260 ms, "101" after 280 ms; -- sharpright, forward

    lbl0: counter port map (
        clk       => clk,
        reset     => count_reset,
        count_out => count
    );
    lbl1: controller port map (
        clk         => clk,
        reset       => reset,
        sensor_l    => sensors(2),
        sensor_m    => sensors(1),
        sensor_r    => sensors(0),
        count_in    => count,
        count_reset => count_reset,
        motor_l_reset     => motor_l_reset,
        motor_l_direction => motor_l_direction,
        motor_r_reset     => motor_r_reset,
        motor_r_direction => motor_r_direction,
        leds => leds
    );
    lbl_pwm_l: pwm_generator port map (
        clk       => clk,
        reset     => motor_l_reset,
        direction => motor_l_direction,
        count_in  => count,
        pwm       => pwm_l
    );
    lbl_pwm_r: pwm_generator port map (
        clk       => clk,
        reset     => motor_r_reset,
        direction => motor_r_direction,
        count_in  => count,
        pwm       => pwm_r
    );
end structural;
