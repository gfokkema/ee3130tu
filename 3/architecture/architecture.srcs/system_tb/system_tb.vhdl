library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity system_tb is
end system_tb;

architecture structural of system_tb is
    component system is
        port (
            clk      : in std_logic;
            sensor_l : in std_logic;
            sensor_m : in std_logic;
            sensor_r : in std_logic;
            reset    : in std_logic;
            pwm_l    : out std_logic;
            pwm_r    : out std_logic
        );
    end component system;
    
    signal clk : std_logic;
    signal reset : std_logic;
    signal sensors : std_logic_vector (2 downto 0);
    signal pwm_l : std_logic;
    signal pwm_r : std_logic;
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset <= '1' after 0 ns, '0' after 11 ns;
    sensors <= "000" after 0 ms, "101" after 5 ms, "000" after 20 ms,
               "100" after 40 ms, "110" after 60 ms, -- left, sharpleft
               "001" after 80 ms, "011" after 100 ms, -- right, sharpright
               "010" after 120 ms, "111" after 140 ms; -- turn signal, line
               
    
    lbl0: system port map (
        clk => clk,
        reset => reset,
        sensor_l => sensors(2),
        sensor_m => sensors(1),
        sensor_r => sensors(0),
        pwm_l => pwm_l,
        pwm_r => pwm_r
    );
        

end;
