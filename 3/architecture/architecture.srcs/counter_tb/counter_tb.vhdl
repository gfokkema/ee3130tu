library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity counter_tb is
end counter_tb;

architecture structural of counter_tb is
    component counter is
        port (
            clk       : in  std_logic;
            reset     : in  std_logic;
            count_out : out std_logic_vector (23 downto 0)
        );
    end component counter;
    signal clk       : std_logic;
    signal reset     : std_logic;
    signal count_out : std_logic_vector (23 downto 0);
begin
    clk       <= '1' after 0 ns,
                 '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    reset     <= '1' after 0 ns,
                 '0' after 11 ns,
                 '1' after 100 ns,
                 '0' after 111 ns;
    
    lbl0: counter port map (
        clk       => clk,
        reset     => reset,
        count_out => count_out
    );
end architecture structural;
