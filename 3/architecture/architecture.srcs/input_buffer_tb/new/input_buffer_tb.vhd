library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity input_buffer_tb is
end input_buffer_tb;

architecture structural of input_buffer_tb is
    component input_buffer is
        port (
            clk : in std_logic;
            sensor_l_in  : in    std_logic;
            sensor_m_in  : in    std_logic;
            sensor_r_in  : in    std_logic;

            sensor_l_out : out    std_logic;
            sensor_m_out : out    std_logic;
            sensor_r_out : out    std_logic
        );
    end component input_buffer;

    signal clk : std_logic;
    signal sensor_in : std_logic_vector(2 downto 0);
    signal sensor_out : std_logic_vector(2 downto 0);
begin
    clk <= '1' after 0 ns,
           '0' after 5 ns when clk /= '0' else '1' after 5 ns;
    sensor_in <= "000" after 0 ns,
                 "001" after 25 ns,
                 "010" after 45 ns;

    lbl0: input_buffer port map(
        clk => clk,
        sensor_l_in => sensor_in(2),
        sensor_m_in => sensor_in(1),
        sensor_r_in => sensor_in(0),
        sensor_l_out => sensor_out(2),
        sensor_m_out => sensor_out(1),
        sensor_r_out => sensor_out(0)
    );
end structural;
